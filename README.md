# Blocklists

My custom blocklists

## Why UCEPROTECT is on my blocklist

This blacklist company acts illegal in many ways - if you research the company on the German internet, you will find out that the founder basically doesn't act after the law, uses aliases and moved many times.

There is a reason why the blacklist is the only one under the "Suspect RBL providers" category on Wikipedia.

See https://uceprotect.wtf/

## Report a new SPAM / Malware / etc Domain

* Get the domain registrar, DNS provider and hoster from whois lookup
* Report the domain to the registrar
* Report the website to the hoster and / or the DNS provider like Cloudflare
* Add the domain to this hosts file

## Report SPAM Mail / etc

* Look at the Header of the Mail
* Search after the X-Report-Abuse-To mail address and report the SPAM there
* Search the Header after the Mailserver which has sent the Mail
* Look after links in the body of the Mail
* Report Domains
* Services like MailChimp, SendGrid and Microsoft have thier own Report pages

## Report SPAM to

* https://www.spamcop.net/
* Report Spam to Google: https://www.google.com/webmasters/tools/spamreport
* Report Malware to Google: https://safebrowsing.google.com/safebrowsing/report_badware/
* https://www.malwaredomainlist.com/forums/
* https://urlhaus.abuse.ch/
* Report Spam to SendGrid: https://sendgrid.com/report-spam/
* https://www.abuseipdb.com/

## Annoy spammers

* https://github.com/beweinreich/mlooper
* http://spa.mnesty.com/

## Report SCAM phone numbers

* https://spamcalls.net/en/page/report-spam-calls
* https://whosenumber.info/
* https://reportfraud.ftc.gov/#/form/main
* https://www.actionfraud.police.uk/
* https://www.bundesnetzagentur.de/DE/Vportal/AnfragenBeschwerden/Beschwerde_Aerger/start.html
